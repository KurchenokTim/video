﻿using System;
using System.Collections.Generic;

namespace Home_Task_Interface_30
{
    public class Program
    {
        static void Main(string[] args)
        {
           
        Student tim = new Student("Tim");
        tim.CanDrive();
        Worker Alex = new Worker("Developer");
         Alex.CanDrive();
         SchoolBoy slaker = new SchoolBoy();
         slaker.CanNotDrive();
        IDriver[] canDrive = { new Student("Tim"),new Worker(".NET C# Developer") };
        WhoCanDrive(canDrive);
        }
        static void WhoCanDrive(IDriver[] canDrive)
        {
            foreach (IDriver drive in canDrive)
            {
                Console.WriteLine($"Auto {} can drive  and ");
                Console.ReadLine();
            }
        }
    }
}
