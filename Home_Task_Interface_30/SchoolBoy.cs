﻿using System;
using System.Collections.Generic;

namespace Home_Task_Interface_30
{
    public class SchoolBoy: People
    {
        public void CanNotDrive()
        {
            Console.WriteLine("SchoolBoy can't drive");
            Console.ReadLine();
        }
    }
}
