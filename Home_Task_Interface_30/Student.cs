﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home_Task_Interface_30
{
    public class Student : People, IDriver
    {
        public string name { get; set; }
        public Student(string name)
        {
            this.name = name;
        }
        public void CanDrive()
        {
            Console.WriteLine($"Student by name {name}");
            Console.ReadLine();
        }

    }
}
