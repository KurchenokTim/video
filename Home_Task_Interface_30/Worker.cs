﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home_Task_Interface_30
{
    public class Worker : People,IDriver
    {
        public string nameSpesiality { get; set; }
        public Worker(string nameSpesiality)
        {
            this.nameSpesiality = nameSpesiality;
        }
        public void CanDrive()
        {
            Console.WriteLine($"Worker by name {nameSpesiality} can drive");
            Console.ReadLine();
        }
    }
}
