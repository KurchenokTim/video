﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface_My_Project
{
    public class StudentWithGoal : Student, IMoney, ITProger
    {
        public string MethodWhoGiveYouMoney()
        {
            return "600$";
        }
        public double HowManyDollorsYouHaveSalary()
        {
            return 500;
        }
    }
}
