﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modificators
{
    public class ClassA
    {
       public  int Property1 { get; set; }
        internal  int Property2 { get; set; }
        //Protected используется для цепочки наследования ,с целью того чтобы тот или 
        //иной элемен был доступен только в цепочки наследования, во внешнем коде он не доступен
       //internal внутренний исп такой модификаторэлементы бдует лоступны только внутри нашей сборки
        protected int Property3 { get; set; }

        //Модификатор досутпа sealed не позволяет наследоваться от класса в котором он прописан
    }
}
