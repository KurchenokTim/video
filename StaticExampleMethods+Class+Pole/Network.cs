﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticExampleMethods_Class_Pole
{
    public class Network
    {
        public string maskAdress;//   255.265.342.325/24
        public string proxyPassword;//cXrM9v: 2Z2ED73kY5"
        public static string proxy;

        public static string Proxy
        {
            get { return proxy;}
            set {  proxy = value;}
        }
        static Network()
        {
            proxy ="109.248.138.37:3000";
        }
        public Network(string maskAdress, string proxyPassword)
        {
            this.maskAdress = maskAdress;
            this.proxyPassword = proxyPassword;
        }
        public void CheckInformationAboutProxy()
        {
            Console.WriteLine($"Password from proxy  {proxyPassword} mask network {maskAdress} to get around system security ");
            Console.ReadKey();
        }
    }
}
