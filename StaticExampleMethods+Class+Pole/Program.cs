﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticExampleMethods_Class_Pole
{
    public class Program
    {
        static void Main(string[] args)
        {
            Network fbr = new Network("255.265.342.325 / 24", "cXrM9v: 2Z2ED73kY5");
            fbr.CheckInformationAboutProxy();
            Network.proxy = "46.8.22.68:3000";
            string[] AllProxy = { "109.248.138.37:3000:cXrM9v: 2Z2ED73kY5", "188.130.142.14:3000:cXrM9v: 2Z2ED73kY5" };
            //Можно вызывать методы на уровне класса не создавая объект этого класса .Благодаря использованию статического Класса
            StaticIP.ProxyCecker(AllProxy);

        }
    }
}
