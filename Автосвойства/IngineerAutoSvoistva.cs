﻿ using System;

namespace ClassUrok
{
    public class IngineerAutoSvoistva
    {
        private string Name { get; set; }
        //Автосвойства позволяют присваивать значения опр поля,чтобы прописыватьлогику
        //для какого-нибудеь поля нужно использтвать обычные свойства
        //используются когда не используется никакая логика
        private string Sername { get; set; }
        private double carearLevel;
        /// <summary>
        /// реализация свойства CareerLevel
        /// </summary>
        public double CareerLevel
        {
            //свой
            //get - брать отдавать
            get { return carearLevel; }
            //получать\
            //Прописываем в нашем свойстве ограничения.Бизнес-правило
            set
            {
                if (value > 180)
                {
                    value = 180;
                }
                carearLevel = value;
            }
        }
        private double howManyYersWorked { get; set; }
        private static double howSalary { get; set; }
        //Переносим инициализацию поля в Статический конструктор
        static IngineerAutoSvoistva()
        {
            howSalary = 1000;
        }
        public IngineerAutoSvoistva()
        {

        }
        //Урок константы
        //Константы не инициализируются в конструкторе класса
        //подразумеватся что поле будет статическим,константа будет доступна для всего класса.Константные поля можно прописывать модификатор доступа public т.к из внешего кода мы никак не сможем именить
        public const double NumberPi = 3.1415252424;
        //Конструктор вызывается неявно.По умолчанию этот метод не видим и не отображается в классе
        public IngineerAutoSvoistva(string Name, string sername, double carearLevel, double howManyYersWorked)
        {
            this.Name = Name;
            this.Sername = sername;
            this.carearLevel = carearLevel;
            this.howManyYersWorked = howManyYersWorked;
            //this.howSalary = howSalary;
        }
        public void WhenEngineerGoToWork()
        {
            Console.WriteLine($"Инжинер по имени {Name}  и фамилией {Sername}, с карьерным уровнем {carearLevel} , идёт на работу в <EPAM>");
        }
        public void Biography()
        {
            Console.WriteLine($"Инжинер по имени {Name}  и фамилией {Sername}, с карьерным уровнем {carearLevel} получает зарплату {howSalary} и работает в компании инжинером {howManyYersWorked}");
        }
        //реализация статического метода
        public static void PrintCabinetNumber()
        {
            Console.WriteLine("В классе определен один стаический констурктор");
        }
    }

}
