﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Вложенные_Классы
{
    public class Computer
    {
        public int HowManyComputersBuild { get; set; }

            public class MotherBord
            {
                public double NameCopany { get; set; }
                public double HowManyRowsMamory { get; set; }

                public void TurnOnMotherBord()
                {
                    Console.WriteLine("Motherbord working good!");
                    Console.ReadKey();
            }
            }

            public class Processor
             {
                public int HowManyCors { get; set; }
                public void TDPCalculate(double tdp)
                 {
                    Console.WriteLine($"The CPU is hot becouse tdp is {tdp} W");

                     Console.ReadKey();
                 }
             }
          public Processor CompOverclocking { get; set; }
          public MotherBord HowMatchCost { get; set; } 

        //Инициализируем автосвойства нашего класса Computer т.к если этого не сделать то будет null
        public Computer()
        {
            CompOverclocking = new Processor();
            HowMatchCost = new MotherBord();
        }
    }
}
