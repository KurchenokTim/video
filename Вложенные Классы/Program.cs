﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Вложенные_Классы
{
    class Program
    {
        static void Main(string[] args)
        {
            Computer MacBook = new Computer();
            MacBook.HowManyComputersBuild = 12;
            MacBook.CompOverclocking.HowManyCors = 8;
            MacBook.CompOverclocking.TDPCalculate(45);
            MacBook.HowMatchCost.HowManyRowsMamory = 4;
            MacBook.HowMatchCost.TurnOnMotherBord();
        }
        //Абстрактные члены могут использоваться только в абстрактном классе
    }
}
//Неявное приведение
//Явное приведение
