﻿using System;


namespace Домашнее_Задание_Классы_Полиморфизм_Фигура
{
    public class FiveAngel : Mnogougolnik, IPoints
    {
        public string NameFigure = "FiveAngel";
        public override void MethodDraw()
        {
            Console.WriteLine($"Name Figure is {NameFigure}");
            Console.WriteLine($"Draw Figure {NameFigure}");
        }
        public int CalculatePoints()
        {
            return 5;
        }
    }
}
