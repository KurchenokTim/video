﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Домашнее_Задание_Классы_Полиморфизм_Фигура
{
    public class FourAngel : Mnogougolnik, IPoints
    {
        public string NameFigure = "FourAngel";
        public override void MethodDraw()
        {
            Console.WriteLine($"Name Figure is {NameFigure}");
            Console.WriteLine($"Draw Figure {NameFigure}");
        }
        public int CalculatePoints()
        {
            //return ("FourAngel gave 4 points");
            return 4;

        }
    }

}
