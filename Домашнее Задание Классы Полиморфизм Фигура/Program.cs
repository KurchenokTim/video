﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Домашнее_Задание_Классы_Полиморфизм_Фигура
{
    public class Program
    {
        static void Main(string[] args)
        {
            Circle firstFigure = new Circle();
            firstFigure.MethodDraw();
            Triangle firstFigure1 = new Triangle();
            firstFigure1.CalculatePoints();
            FourAngel firstFigure32 = new FourAngel();
            firstFigure32.CalculatePoints();
            Figure[] figures = { new Triangle(), new Circle(), new FiveAngel(), new FourAngel(), new Oval() };
            Print(figures);
            IPoints[] shapes = { new Triangle(), new FiveAngel(), new FourAngel() };
            FindFigureWithMaxpoints(shapes);
            Console.ReadKey();
        }

        static void Print(Figure[] figures)
        {
            foreach (Figure figura in figures)
            {
                figura.MethodDraw();
                if (figura is IPoints)
                    Console.WriteLine($"In this figure {((IPoints)figura).CalculatePoints()} points");
            }
            Console.ReadKey();
        }

        static void FindFigureWithMaxpoints(IPoints[] shapes)
        {
            int max = 0;
            foreach (IPoints shape in shapes)
            {
                if (shape.CalculatePoints() > max)
                {
                    max = shape.CalculatePoints();
                }
            }
            Console.WriteLine($"Figure with max points have {max} points");  
        }
    }
}
