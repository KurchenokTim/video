﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Домашнее_Задание_Классы_Полиморфизм_Фигура
{
     public  class Triangle : Mnogougolnik, IPoints
    {
        public string NameFigure = " Triangle";
        public override void MethodDraw()
        {
            Console.WriteLine($"Name Figure is {NameFigure}");
            Console.WriteLine($"Draw Figure {NameFigure}");
        }
        public int CalculatePoints()
        {
            //return "Triangel gave tree points";
            return 3;
        }
    }
}
