﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Наследование_Через_Включение
{
    public class Table
    {
        public int Number { get; set; }
            public class Computer
            {
                public int SerialNumber { get; set; }
            public  void TurnOn()
            {

            }
            }
        //Класс - это ссылочный тип данных.если не прописать значение в констуркторе то наше автосвойство будет пустым
        public Computer CompSetttings { get; set; }
        public Table()
        {
            //иницаилизируем автосвойство
            CompSetttings = new Computer();
        }
    }
}
