﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Наследование
{
    public class Animal
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int Weight { get; set; }


        public Animal(string name, int age, int weight)
        {
            Weight = weight;
            Name = name;
            Age = age;
            
        }
        public virtual void Feed() { }
    }


}
