﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Наследование
{
    public class Elephant : Animal
    {
        public Elephant(string name, int age, int weight) : base(name, weight, age)
        { }
        public override void Feed()
        {

            //base.Feed(); означает что в начале код выполнится в базовом классе,а только потом в  дочернем
            base.Feed();
            Console.WriteLine("\nСлона мы покормили бананами");
        }
    }
}
