﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Наследование
{
    public class Tiger : Animal
    {
        public int TailLength { get; set; }
        public Tiger(string name , int age ,int weight, int tailLength): base(name, weight, age)
        {
            TailLength = tailLength;
        }
    }
}
