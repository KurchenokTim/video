﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Применение_Системных_Интерфейсов
{
    class Program
    {
        static void Main(string[] args)
        {
            Proger middle = new Proger();
            middle.Name = "Kurchenok Tim Yureivich";
            middle.HowMachSalary = 3000;
            Proger senior = (Proger)middle.Clone();
            Console.WriteLine(senior.Name);
            Console.WriteLine(senior.HowMachSalary);
        }
    }
    class Proger : ICloneable
    {
        public string Name { get; set; }
        public double HowMachSalary { get; set; }
        public object Clone()
        {
            Proger junior = new Proger();
            junior.Name = this.Name;
            junior.HowMachSalary = this.HowMachSalary;
            return junior;
        }
    }
}
