﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassUrok
{
    public class Ingineer
    {
        private string name;
        private string sername;
        private double carearLevel;
        /// <summary>
        /// реализация свойства CareerLevel
        /// </summary>
        public double CareerLevel
        {
            //свой
            //get - брать отдавать
            get { return carearLevel; }
            //получать\
            //Прописываем в нашем свойстве ограничения.Бизнес-правило
            set 
            {
                if (value > 180)
                {
                    value = 180;
                }
                carearLevel = value; 
            }
        }
        private double howManyYersWorked;
        private static double howSalary;
         //Переносим инициализацию поля в Статический конструктор
        static Ingineer()
        {
            howSalary = 1000;
        }
        public Ingineer()
        {

        }
        //Конструктор вызывается неявно.По умолчанию этот метод не видим и не отображается в классе
        public Ingineer(string Name, string sername, double carearLevel, double howManyYersWorked)
        {
            this.name = Name;
            this.sername = sername;
            this.carearLevel = carearLevel;
            this.howManyYersWorked = howManyYersWorked;
            //this.howSalary = howSalary;
        }
        public void WhenEngineerGoToWork()
        {
            Console.WriteLine($"Инжинер по имени {name}  и фамилией {sername}, с карьерным уровнем {carearLevel} , идёт на работу в <EPAM>");
        }
        public void Biography()
        {
            Console.WriteLine($"Инжинер по имени {name}  и фамилией {sername}, с карьерным уровнем {carearLevel} получает зарплату {howSalary} и работает в компании инжинером {howManyYersWorked}");
        }
        //реализация статического метода
        public static void PrintCabinetNumber()
        {
            Console.WriteLine("В классе определен один стаический констурктор");
        }
    }

}
