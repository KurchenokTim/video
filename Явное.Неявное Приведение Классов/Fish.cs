﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Явное.Неявное_Приведение_Классов
{
    public class Fish :  object
    {
        public int Veight {get; set; }
        public int HowManyHarts {get; set;}

        public Fish(int Veight ,int HowManyHarts)
        {
            this.Veight = Veight;
            this.HowManyHarts = HowManyHarts;
        }
    }
}
