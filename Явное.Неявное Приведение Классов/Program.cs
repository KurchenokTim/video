﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Явное.Неявное_Приведение_Классов
{
    public class Program
    {
        static void Main(string[] args)
        {
            Fish fish1 = new Salmon(124,134);
            WhoAreYou(fish1);
            Fish fish2 = new Tunec(124, 134);
            WhoAreYou(fish2);
            Fish fish = new Salmon(10,13);
            //Fish spirit = new Tunec(10, 13);
            //Реализация явного приведение Класса
            //мы не можем использовать метод  WhatColorFish потому что в качестве параметра этот метод ожидает 
            //парматер типа Salmon но мы передаем объект класса Fish,для того чтобы тип был которые нам нужен нам нужно использовать явное приедение Класов
            WhatColorFish((Salmon)fish);

        }
        static void WhoAreYou(Fish fish)
        {
            
            if (fish is Salmon)
            {
                Console.WriteLine("Yes?this is clear type Salmon");
                
            }
            if (fish is Tunec)
            {
                Console.WriteLine("Yes?this is clear type Tunec");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Go away !!");
            }
            
        }

        static void WhatColorFish(Salmon salmon)
            {
        //    if (salmon is Salmon)
        //    {
        //        Console.WriteLine("Yes?this is clear type");

        //    }
            if (salmon as Salmon != null)
            {
                Console.WriteLine("Yes?this is clear type");
            }
            else
            {
                Console.WriteLine("Go away !!");
            }
            Console.WriteLine("Red");
        }
    }
}
